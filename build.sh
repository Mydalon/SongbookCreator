#!/bin/bash

#Copy external mount away for working on it
rm -rf /mnt/music_internal
mkdir /mnt/music_internal
cp -r /mnt/music /mnt/music_internal
cd /mnt/music_internal

#Create directories
mkdir pdf_output

#Render CRD files
find -name '*.crd' -execdir chordpro -o {}.pdf --text-size=14 --chord-size=12 --config=modern3 {} \;

#Render MuseScore files
export QT_QPA_PLATFORM=offscreen
find -name '*.mscz' -execdir musescore -S songbook.mss -o {}.pdf {} \;

#Transform abc to lilypond
find -name '*.abc' -execdir abc2ly {} \;

#Render lilypond files
find -name '*.ly' -execdir lilypond {} \;

#Collect all pdf and tex files to build the book
find music -name '*.pdf' \! -path './pdf_output/*' \! -name 'Songbook.pdf' -exec cp --parents {} pdf_output/ \;
find -name '*.tex' \! -path './pdf_output/*' -exec cp --parents {} pdf_output/ \;

#Create .tex file for songbook
find pdf_output -name '*.pdf' |sort|/bin/create_book.sh > Songbook.tex

#Run three times to make sure TOC is correct
pdflatex Songbook.tex
pdflatex Songbook.tex
pdflatex Songbook.tex

mv Songbook.pdf /mnt/music/Songbook.pdf
rm -rf /mnt/music_internal
