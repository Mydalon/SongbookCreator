FROM ubuntu 
RUN apt-get update
RUN apt-get -y install software-properties-common
RUN add-apt-repository ppa:mscore-ubuntu/mscore-stable
RUN apt-get update
RUN apt-get -y install lilypond
RUN apt-get -y install make gcc
RUN apt-get -y install cpanminus
RUN cpanm install chordpro

RUN apt-get -y install texlive-latex-base
RUN apt-get -y install texlive-latex-recommended
RUN apt-get -y install texlive-lang-german

RUN apt-get -y install poppler-utils
RUN apt-get -y install abcm2ps

RUN apt-get -y install musescore
RUN apt-get -y install exiftool

RUN mkdir /usr/lib/x86_64-linux-gnu/fonts

COPY build.sh /bin/build.sh
COPY create_book.sh /bin/create_book.sh
COPY songbook.mss /var/songbook/songbook.mss

CMD /bin/build.sh

