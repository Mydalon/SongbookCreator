echo "% songbook.tex
\documentclass[a4paper,12pt]{article}

\usepackage{pdfpages}
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}
\DeclareGraphicsRule{*}{pdf}{*}{}

\begin{document}
\tableofcontents
\pagebreak
"
old_directory=''
while read filename
do
  directory=`echo $filename|awk 'BEGIN{FS="/"} {print $(NF-1)}'`
  if [ "$directory" != "$old_directory" ]; then
    dir=$(dirname "${filename}")
    texfile="$dir/000Intro.tex"
    if [ -e "$texfile" ]; then
      echo "\input{$texfile}"
    else
      echo "\section{$directory}"
    fi
    old_directory=$directory
  fi
  title=`exiftool $filename |grep "Title"|awk 'BEGIN{FS=":"} { print $2 }'|awk '{$1=$1};1'`
  echo "\includepdf[pages=-, pagecommand={}, addtotoc={1,subsection,2,{$title},{$filename}}]{$filename}"
done
echo "\end{document}"
